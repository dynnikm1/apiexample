import { createRouter, createWebHashHistory } from 'vue-router'
import ProductsIndexView from '../views/ProductsIndexView.vue'
import StoreIndexView from '../views/StoreIndexView.vue'
import ProductShowView from '../views/ProductShowView.vue'
import StoreShowView from '../views/StoreShowView.vue'
import StoreProductsView from '../views/StoreProductsView.vue'

const routes = [
  {
    path: '/',
    name: 'products',
    component: ProductsIndexView
  },
  {
    path: '/stores',
    name: 'stores',
    component: StoreIndexView
  },
  {
    path: '/store/:store_id/product/:id',
    name: 'productshow',
    component: ProductShowView
  },
  {
    path: '/store/:id',
    name: 'storeshow',
    component: StoreShowView
  },
  {
    path: '/stores/:store_id/products',
    name: 'storeproducts',
    component: StoreProductsView
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router

